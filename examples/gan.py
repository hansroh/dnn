# # another implementation of https://github.com/hwalsuklee/tensorflow-mnist-VAE

import dnn
from dnn import split, predutil, plot, priors, optimizers
from dnn.costs import gan, cosine_distance_error
from datasets.mnist import read_data_sets
from tqdm import tqdm
import tensorflow as tf
import numpy as np
from rs4 import pathtool

class Generator (dnn.DNN):
    dim_z = 2

    def discriminator (self, x):
        with tf.compat.v1.variable_scope ("discriminator", reuse = tf.compat.v1.AUTO_REUSE):
            layer = self.dense (x, 512, activation = tf.nn.relu)
            layer = self.dropout (layer)
            layer = self.dense (layer, 512, activation = tf.nn.relu)
            layer = self.dropout (layer)
            logit = self.dense (layer, 1, activation = tf.sigmoid)
        return logit

    def generator (self, x):
        with tf.compat.v1.variable_scope ("generator", reuse = tf.compat.v1.AUTO_REUSE):
            layer = self.dense (x, 512, activation = tf.nn.tanh)
            layer = self.dropout (layer)
            layer = self.dense (layer, 512, activation = tf.nn.elu)
            layer = self.dropout (layer)
            output = self.dense (layer, 784, activation = tf.sigmoid)
        return output

    def make_place_holders (self):
        self.x = tf.compat.v1.placeholder ("float", [None, 784])
        self.y = tf.compat.v1.placeholder ("float", [None, self.dim_z])

    def make_logit (self):
        return self.generator (self.y)

    def make_optimizer (self):
        return [
            self.optimizer ("adam", self.scoped_cost (self.D_loss, "discriminator"), self.learning_rate / 5),
            self.optimizer ("adam", self.scoped_cost (self.G_loss, "generator")),
            self.optimizer ("adam", self.scoped_cost (self.G_loss, "generator"))
        ]

    def make_cost (self):
        self.D_loss, self.G_loss = gan.gan_loss (self.discriminator, self.logit, self.x)
        self.log ("G-loss", self.G_loss)
        self.log ("D-loss", self.D_loss)
        return self.D_loss + self.G_loss


def main (test = False):
    mnist = read_data_sets ("data", one_hot=True, test = test)
    net = Generator ()
    net.set_tensorboard_dir ("./temp/tflog")
    net.set_learning_rate (1e-3, 0.99, 100)

    plotter = plot.Plot_Reproduce_Performance("./temp/results", 8, 8, 28, 28, 1.0)
    plotter.reset ()
    valid_xs = mnist.test.images [0:64, :]
    plotter.save_images(valid_xs.reshape  (64, 28, 28), name='input.jpg')
    valid_xs_noised = plotter.add_noise (valid_xs)
    plotter.save_images(valid_xs_noised.reshape  (64, 28, 28), name='input-noised.jpg')
    train_minibatches = split.minibatch (mnist.train.images, mnist.train.labels, 128)

    training_epochs = test and 3 or 200
    batch_size = 128

    for epoch in range (1, training_epochs + 1):
        print ("\n* Epoch progress: {} / {} ({:.0f}%)".format (epoch, training_epochs, epoch / training_epochs * 100))
        net.set_epoch (epoch)
        total_batch = int (mnist.train.num_examples/batch_size)
        for i in range(total_batch):
            reals, _ = next (train_minibatches)
            batch_zs = priors.gaussian (reals.shape [0], net.dim_z)
            net.batch (reals, batch_zs, dropout_rate = 0.1)

        r = net.valid (reals, batch_zs).log ()
        plotter.save_images (r.logit [:64].reshape  (-1, 28, 28), name = "/PRR_epoch_%02d" %(epoch) + ".jpg")

if __name__ == "__main__":
    import sys

    main ("--test" in sys.argv)

