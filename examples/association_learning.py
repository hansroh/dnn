# another implementation of https://github.com/haeusser/learning_by_association

import dnn
from dnn import split, predutil
from dnn.costs import association
from datasets.mnist import read_data_sets, resample_per_label
from tqdm import tqdm
import tensorflow as tf
import numpy as np

class MNI (dnn.DNN):
    def make_place_holders (self):
        self.x = tf.compat.v1.placeholder ("float", [None, 784])
        self.y = tf.compat.v1.placeholder ("float", [None, 10])
        self.u = tf.compat.v1.placeholder_with_default (tf.zeros ([1, 784]), [None, 784])
        self.visit_weight = tf.compat.v1.placeholder_with_default (tf.constant (1.0), [])
        self.walker_weight = tf.compat.v1.placeholder_with_default (tf.constant (1.0), [])

    def make_optimizer (self):
        return self.optimizer ("adam")

    def embed (self, x):
        x = tf.reshape (x, [-1, 28, 28, 1])
        with tf.compat.v1.variable_scope ("embedding", reuse = tf.compat.v1.AUTO_REUSE):
            net = self.conv2d (x, 32, [3, 3], activation = tf.nn.elu, kreg = self.l2 (1e-3))
            net = self.conv2d (net, 32, [3, 3], activation = tf.nn.elu, kreg = self.l2 (1e-3))
            net = self.max_pool2d(net, [2, 2])  # 14
            net = self.conv2d(net, 64, [3, 3], activation = tf.nn.elu, kreg = self.l2 (1e-3))
            net = self.conv2d(net, 64, [3, 3], activation = tf.nn.elu, kreg = self.l2 (1e-3))
            net = self.max_pool2d(net, [2, 2])  # 7
            net = self.conv2d(net, 128, [3, 3], activation = tf.nn.elu, kreg = self.l2 (1e-3))
            net = self.conv2d(net, 128, [3, 3], activation = tf.nn.elu, kreg = self.l2 (1e-3))
            net = self.max_pool2d(net, [2, 2])  # 3
            net = self.full_connect (net)
            emb = self.dense (net, 128, activation = tf.nn.elu)
        return emb

    def embedding2logit (self, x):
        with tf.compat.v1.variable_scope ("logit", reuse = tf.compat.v1.AUTO_REUSE):
            return self.dense (x, 10, kreg = self.l2 (1e-4))

    def make_cost (self):
        b = self.embed (self.u)
        loss_w, loss_v, est_err = association.loss (
            self.embeded, b,
            tf.argmax (input=self.y, axis = 1),
            self.visit_weight, self.walker_weight
        )
        self.log ("walker-loss", loss_w)
        self.log ("visit-loss", loss_v)
        self.log ("estimate-error", est_err)
        loss_logit = tf.reduce_mean (input_tensor=tf.nn.softmax_cross_entropy_with_logits (
            labels = self.y, logits = self.logit
        ))
        return loss_w + loss_v + loss_logit

    def make_logit (self):
        self.embeded = self.embed (self.x)
        return self.embedding2logit (self.embeded)

    def performance_metric (self, r):
        return r.metric.f1.weighted


def main (test = False):
    mnist = read_data_sets ("data", one_hot=True, test = test)
    net = MNI ()
    net.set_tensorboard_dir ("./temp/tflog")

    train_xs = mnist.train.images
    test_xs = mnist.test.images

    print ("  - Validation data mean {:.2f}, std {:.2f}, min {:.2f}, max {:.2f}".format (test_xs.mean (), test_xs.std (), test_xs.min (), test_xs.max ()))
    net.set_learning_rate (1e-3, 0.99, 300)

    training_epochs = test and 3 or 200
    batch_size = 100
    labels_per_class = 10
    batch_labels_per_class = 10 # per class
    n_labels = 10
    labeled_xs, labeled_ys = resample_per_label (train_xs, mnist.train.labels, labels_per_class, n_labels)
    train_minibatches = split.minibatch (train_xs, mnist.train.labels, batch_size)
    # just make sure all labels is contained oer batch
    labeled_minibatches = split.minibatch (labeled_xs, labeled_ys,  n_labels * batch_labels_per_class, rand = False)
    for epoch in range (1, training_epochs + 1):
        print ("\n* Epoch progress: {} / {} ({:.0f}%)".format (epoch, training_epochs, epoch / training_epochs * 100))
        total_batch = mnist.train.num_examples // batch_size
        net.set_epoch (epoch)
        for i in range (total_batch):
            batch_xs, _ = next (train_minibatches)
            label_xs, label_ys = next (labeled_minibatches)
            net.batch (label_xs, label_ys, u = batch_xs, visit_weight = 1.0)
        net.resub (labeled_xs, labeled_ys).log ()
        r = net.valid (test_xs, mnist.test.labels).log ()
        r.confusion_matrix (10)

        net.cherry.log ()
        net.cherry.confusion_matrix (10)

        if net.is_overfit:
            print ("* Overfit in progress, terminated.")
            break

if __name__ == "__main__":
    import sys

    main ("--test" in sys.argv)
